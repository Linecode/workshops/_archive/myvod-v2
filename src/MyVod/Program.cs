using System.Reflection;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Data;
using MyVod.Domain;
using MyVod.Infrastructure;
using MyVod.Infrastructure.Legal;
using MyVod.Infrastructure.Movies;
using MyVod.Services;
using Stripe;
using OrderService = MyVod.Services.OrderService;
using PersonService = MyVod.Services.PersonService;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connectionString));
builder.Services.AddDbContext<MoviesDbContext>(options =>
{
    options.UseSqlite(connectionString, 
        x => x.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
});
builder.Services.AddDbContext<MoviesDbContextV2>(options =>
{
    options.UseSqlite(connectionString,
        x => x.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
});
builder.Services.AddDbContext<LegalDbContext>(options =>
{
    options.UseSqlite(connectionString,
        x => x.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
});
builder.Services.AddDatabaseDeveloperPageExceptionFilter();
builder.Services.AddMediatR(typeof(DomainAssemblyInfo));

builder.Services.Scan(scan =>
    scan.FromAssemblies(typeof(DomainAssemblyInfo).Assembly, typeof(InfrastrucutreAssemblyInfo).Assembly)
        .AddClasses(classes => classes.AssignableTo<IApplicationService>())
            .AsImplementedInterfaces()
            .WithScopedLifetime()
        .AddClasses(classes => classes.AssignableTo<IRepository>())
            .AsImplementedInterfaces()
            .WithScopedLifetime());

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<ApplicationDbContext>();
builder.Services.AddControllersWithViews()
    .AddRazorRuntimeCompilation();


StripeConfiguration.ApiKey =
    "sk_test_51L3x0jIP4aoyQ2vi0RgDKWCPmI22ugYLYJbCeUBrgfYMEE0HBegyc5sJDt06xeEc1PQVtXqv56by02gLDwXZyU8Q00QT28pWGf";

builder.Services.AddScoped<IMovieService, MovieService>();
builder.Services.AddScoped<IGenreService, GenreService>();
builder.Services.AddScoped<IPersonService, PersonService>();
builder.Services.AddScoped<IOrderService, OrderService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "MyArea",
    pattern: "{area:exists}/{controller=Dashboard}/{action=Index}/{id?}");
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();
