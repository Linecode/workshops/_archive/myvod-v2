using Microsoft.AspNetCore.Mvc;
using MyVod.Domain.Legal.Application;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.SharedKernel;
using MyVod.Infrastructure.Models;
using MyVod.Services;

namespace MyVod.Controllers;

public class MoviesController : Controller
{
    private readonly IMovieService _movieService;
    private readonly Domain.MoviesCatalog.Application.IMovieService _movieServiceV2;
    private readonly ILicenseService _licenseService;

    public MoviesController(IMovieService movieService, 
        Domain.MoviesCatalog.Application.IMovieService movieServiceV2, ILicenseService licenseService)
    {
        _movieService = movieService;
        _movieServiceV2 = movieServiceV2;
        _licenseService = licenseService;
    }

    [HttpGet("[controller]/{id:guid}")]
    [ActionName("")]
    public async Task<IActionResult> Index(Guid id)
    {
        var movie = await _movieService.Get(id);   
        return View("Index", movie);
    }

    [HttpGet("/movieee")]
    // ReSharper disable once RouteTemplates.ParameterTypeAndConstraintsMismatch
    public async Task<IActionResult> Test()
    {
        var movieId = MovieId.Parse("53341AD3-0185-4ECD-80B8-9F12FEA96370");

        // var movie = await _movieServiceV2.Get(movieId);
        var dateRange = DateRange.Create(DateTime.UtcNow.Subtract(TimeSpan.FromDays(3)),
            DateTime.UtcNow.Add(TimeSpan.FromDays(3)));
        var regId = new RegionalIdentifier((ushort)616);
        await _licenseService.Add(dateRange, regId, movieId);
        
        return Json(new {});
    }
}
