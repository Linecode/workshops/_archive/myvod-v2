using MyVod.Infrastructure.Models;

namespace MyVod.Areas.Admin.ViewModels;

public class TransactionsPageModel
{
    public List<Order> OnGoing { get; set; } = new();
    public List<Order> Completed { get; set; } = new();
}