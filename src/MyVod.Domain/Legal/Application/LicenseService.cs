using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.Legal.Infrastructure;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Legal.Application;

public interface ILicenseService : IApplicationService
{
    Task<License> Get(LicenseId id);
    Task Add(DateRange dateRange, RegionalIdentifier regionalIdentifier, MovieId movieId);

    Task Active(LicenseId id);
    Task DeActivate(LicenseId id);
    Task<IEnumerable<License>> FetchReadyToActivate();
}

public class LicenseService : ILicenseService
{
    private readonly ILicenseRepository _repository;

    public LicenseService(ILicenseRepository repository)
    {
        _repository = repository;
    }
    
    public Task<License> Get(LicenseId id)
    {
        throw new NotImplementedException();
    }

    public async Task Add(DateRange dateRange, RegionalIdentifier regionalIdentifier, MovieId movieId)
    {
        var license = new License(dateRange, movieId, regionalIdentifier);
        
        _repository.Add(license);
        await _repository.UnitOfWork.SaveEntitiesAsync();
    }

    public Task Active(LicenseId id)
    {
        throw new NotImplementedException();
    }

    public Task DeActivate(LicenseId id)
    {
        throw new NotImplementedException();
    }

    public async Task<IEnumerable<License>> FetchReadyToActivate()
    {
        var spec = new PassedLicenseStartTimeSecification()
            .And(new NotActiveLicenseSpecification());

        var licenses = await _repository.Get(spec);

        return licenses!;
    }
}