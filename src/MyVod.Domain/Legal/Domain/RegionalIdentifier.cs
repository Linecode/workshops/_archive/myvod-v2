using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Legal.Domain;

// iso: 3166
public class RegionalIdentifier : ValueObject<RegionalIdentifier>
{
    public ushort Value { get; private set; } = UInt16.MinValue;

    [Obsolete("Only for EF", true)]
    public RegionalIdentifier()
    {
    }

    public RegionalIdentifier(ushort id)
    {
        Ensure.That(id).IsInRange((ushort)4,(ushort)999);

        Value = id;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
}