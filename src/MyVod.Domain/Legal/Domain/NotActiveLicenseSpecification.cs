using System.Linq.Expressions;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Legal.Domain;

public class NotActiveLicenseSpecification : Specification<License>
{
    public override Expression<Func<License, bool>> ToExpression()
        => license => license.Status == License.LicenseStatus.NotActive;
}

public class PassedLicenseStartTimeSecification : Specification<License>
{
    public override Expression<Func<License, bool>> ToExpression()
        => license => license.DateRange.EndDate <= DateTime.UtcNow;
}