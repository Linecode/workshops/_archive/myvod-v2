using System.ComponentModel;
using System.Globalization;
using Newtonsoft.Json;

namespace MyVod.Domain.Legal.Domain;

[TypeConverter(typeof(LicenseIdTypeConverter))]
[JsonConverter(typeof(LicenseIdJsonConverter))]
public readonly struct LicenseId : IComparable<LicenseId>, IEquatable<LicenseId>
{
    public Guid Value { get; }
    
    public static LicenseId New() => new(Guid.NewGuid());
    public static LicenseId Parse(string id) => new(Guid.Parse(id)); 

    public LicenseId(Guid value) => Value = value;

    public bool Equals(LicenseId other) => Value.Equals(other.Value);
    public override bool Equals(object obj) => obj is LicenseId other && Equals(other);
    public int CompareTo(LicenseId other) => Value.CompareTo(other.Value);
    public override int GetHashCode() => Value.GetHashCode();
    public override string ToString() => $"LicenseId: {Value.ToString()}";
    public static bool operator ==(LicenseId a, LicenseId b) => a.CompareTo(b) == 0;
    public static bool operator !=(LicenseId a, LicenseId b) => !(a == b);

    private class LicenseIdTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var stringValue = value as string;
            if (!string.IsNullOrEmpty(stringValue)
                && Guid.TryParse(stringValue, out var guid))
            {
                return new LicenseId(guid);
            }

            return base.ConvertFrom(context, culture, value);
        }
    }
    
    private class LicenseIdJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
            => objectType == typeof(LicenseId);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var id = (LicenseId)value;
            serializer.Serialize(writer, id.Value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var guid = serializer.Deserialize<Guid>(reader);
            return new LicenseId(guid);
        }
    }
}