using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.CannonicalModel.Events;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Legal.Domain;

public sealed class License : Entity<LicenseId>, IAggregateRoot<LicenseId>
{
    public DateRange DateRange { get; private set; }
    public MovieId MovieId { get; private set; }
    public LicenseStatus Status { get; private set; } = LicenseStatus.NotActive;
    public RegionalIdentifier RegionalIdentifier { get; private set; }

    [Obsolete("Only for EF", true)]
    private License()
    {
    }

    public License(DateRange dateRange, MovieId movieId, RegionalIdentifier regionalIdentifier)
    {
        DateRange = dateRange;
        MovieId = movieId;
        RegionalIdentifier = regionalIdentifier;
        
        Id = LicenseId.New();

        Raise(new LicenseCreatedEvent { LicenseId = Id });

        if (DateRange.IsWithinRange(DateTime.UtcNow))
        {
            Status = LicenseStatus.Active;
            
            Raise(new LicenseActivatedEvent { LicenseId = Id, MovieId = movieId });
        }
    }

    public void Activate()
    {
        if (DateRange.IsWithinRange(DateTime.UtcNow))
            Status = LicenseStatus.Active;
    }

    public void DeActivate()
        => Status = LicenseStatus.NotActive;

    public enum LicenseStatus
    {
        NotActive,
        Active
    }
}