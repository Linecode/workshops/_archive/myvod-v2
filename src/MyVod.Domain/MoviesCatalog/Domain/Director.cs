using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MoviesCatalog.Domain;

public class Director : Entity<DirectorId>
{
    public string FirstName { get; private set; }
    public string LastName { get; private set; }

    [Obsolete("For EF Core", true)]
    private Director()
    {
    }

    public Director(string firstName, string lastName)
    {
        FirstName = firstName;
        LastName = lastName;
    }
}