using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.MoviesCatalog.Domain;

public class Title : ValueObject<Title>
{
    public string Original { get; private set; }
    public string English { get; private set; }

    [Obsolete("Only for EF Core", true)]
    private Title() {}
    
    private Title(string original, string english)
    {
        Original = original;
        English = english;
    }

    public static Title Create(string original, string english)
    {
        Ensure.That(original).HasLengthBetween(2, 500);
        Ensure.That(english).HasLengthBetween(2,500);

        return new(original, english);
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Original;
        yield return English;
    }
}