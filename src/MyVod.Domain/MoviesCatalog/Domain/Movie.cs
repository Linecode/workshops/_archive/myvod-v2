using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.MoviesCatalog.Domain;

public sealed class Movie : Entity<MovieId>, IAggregateRoot<MovieId>
{
    public Title Title { get; private set; } = null!;

    public Description Description { get; private set; } = null!;
    
    public Uri Cover { get; private set; } = null!;
    public Uri Trailer { get; private set; } = null!;
    
    public MetaData MetaData { get; private set; } = null!;
    
    public byte[] Timestamp { get; private set; } = null!;

    public Director Director { get; private set; } = null!;  

    [Obsolete("Only For EF", true)]
    private Movie()
    { }

    internal Movie(MovieId id)
    {
        Id = id;
    }
    
    public Movie(Title title)
    {
        Title = title;
    }

    public Movie(Title title, Description description)
    {
        Title = title;
        Description = description;
        
        Id = MovieId.New();
    }

    public void AddMetaData(MetaData metaData)
    {
        MetaData = metaData;
    }

    public void DefineMedia(Uri cover, Uri trailer)
    {
        Ensure.That(cover).IsNotNull();
        Ensure.That(trailer).IsNotNull();
        
        Cover = cover;
        Trailer = trailer;
    }
    
    public MovieStatus Status { get; private set; } = MovieStatus.UnPublished;

    public void Publish()
    {
        Status = MovieStatus.Published;
    }

    public void UnPublish()
    {
        Status = MovieStatus.UnPublished;
    }

    public MovieReadModel GetSnapshot() 
        => new() { Title = Title.English, Description = Description.Value, CreateAt = DateTime.UtcNow};

    public enum MovieStatus
    {
        UnPublished,
        Published
    }
}