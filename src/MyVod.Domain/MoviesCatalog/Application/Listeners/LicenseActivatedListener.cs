using MediatR;
using MyVod.Domain.CannonicalModel.Events;

namespace MyVod.Domain.MoviesCatalog.Application.Listeners;

public class LicenseActivatedListener : INotificationHandler<LicenseActivatedEvent>
{
    private readonly IMovieService _movieService;

    public LicenseActivatedListener(IMovieService movieService)
    {
        _movieService = movieService;
    }
    
    public async Task Handle(LicenseActivatedEvent notification, CancellationToken cancellationToken)
    {
        await _movieService.Publish(notification.MovieId);
    }
}