using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.MoviesCatalog.Infrastructure;

namespace MyVod.Domain.MoviesCatalog.Application;

public interface IMovieService : IApplicationService
{
    ValueTask<Movie?> Get(MovieId id);
    Task Publish(MovieId id);
}

public class MovieService : IMovieService
{
    private readonly IMoviesRepository _repository;

    public MovieService(IMoviesRepository repository)
    {
        _repository = repository;
    }
    
    public async ValueTask<Movie?> Get(MovieId id)
    {
        var movie = await _repository.Get(id);

        return movie;
    }

    public async Task Publish(MovieId id)
    {
        var movie = await _repository.Get(id);

        if (movie is null)
            return;
        
        movie.Publish();
        
        _repository.Update(movie);
        await _repository.UnitOfWork.SaveEntitiesAsync();
    }
}