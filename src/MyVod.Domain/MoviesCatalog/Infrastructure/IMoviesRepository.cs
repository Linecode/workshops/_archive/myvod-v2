using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.MoviesCatalog.Domain;

namespace MyVod.Domain.MoviesCatalog.Infrastructure;

public interface IMoviesRepository : IRepository
{
    Task<Movie?> Get(MovieId id);
    void Update(Movie movie);
}