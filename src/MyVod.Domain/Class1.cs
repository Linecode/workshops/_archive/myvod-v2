﻿using System.Numerics;
using EnsureThat;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain;

public class Nip : ValueObject<Nip>
{
    public string Value { get; private set; }
    public string Prefix { get; private set; }

    [Obsolete("Only For EF", true)]
    private Nip(){}
    
    private Nip(string value, string prefix)
    {
        Value = value;
        Prefix = prefix;
    }

    public static Nip Create(string value, string prefix)
    {
        Ensure.That(value).IsNotEmpty();
        Ensure.That(prefix).IsNotEmpty();

        return new(value, prefix);
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
        yield return Prefix;
    }
}

public class Program
{
    public void Main()
    {
        var nip1 = Nip.Create("dsa", "dsad");
        var nip2 = Nip.Create("dsadasdas", "dsadsadsa");

        var a = nip1 != nip2;
    }
}