using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.SharedKernel;

public class MovieReadModel : ValueObject<MovieReadModel>
{
    public string Title { get; init; }
    public string Description { get; init; }
    public DateTime CreateAt { get; set; }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Title;
        yield return Description;
    }
}