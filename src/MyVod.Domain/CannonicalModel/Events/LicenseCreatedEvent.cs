using MediatR;
using MyVod.Domain.Legal.Domain;

namespace MyVod.Domain.CannonicalModel.Events;

public class LicenseCreatedEvent : INotification
{
    public LicenseId LicenseId { get; init; }
}