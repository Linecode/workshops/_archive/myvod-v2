using MediatR;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.MoviesCatalog.Domain;

namespace MyVod.Domain.CannonicalModel.Events;

public class LicenseActivatedEvent : INotification
{
    public LicenseId LicenseId { get; init; }
    public MovieId MovieId { get; init; }
}