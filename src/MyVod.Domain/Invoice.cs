using System.ComponentModel;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;
using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain;

public sealed class Invoice : Entity<string>
{
    public DateOnly CreatedAt { get; private set; } = DateOnly.FromDateTime(DateTime.UtcNow);

    private List<object> _items = new();
    public IReadOnlyList<object> Items => _items.AsReadOnly();
    
    public Invoice(object userContext)
    {
        Id = Guid.NewGuid().ToString();
    }
}
