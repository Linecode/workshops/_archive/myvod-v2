using MyVod.Domain.Legal.Application;
using MyVod.Domain.Legal.Domain;
using MyVod.Domain.MoviesCatalog.Application;
using MyVod.Domain.MoviesCatalog.Domain;
using OpenSleigh.Core;
using OpenSleigh.Core.Messaging;

namespace MyVod.Domain.BusinessProcesses.ActivateLicense;

public record ActivateLicenseSagaState : SagaState
{
    public ActivateLicenseSagaState(Guid id) : base(id)
    {
    }

    public LicenseId LicenseId { get; set; }
    public MovieId MovieId { get; set; }
    public RegionalIdentifier RegionalIdentifier { get; set; }
}

public record StartActivateLicenseSaga(Guid Id, Guid CorrelationId,
    LicenseId LicenseId, MovieId MovieId, RegionalIdentifier RegId) : ICommand;

public record ActivateLicense(Guid Id, Guid CorrelationId) : ICommand;

public record PublishMovie(Guid Id, Guid CorrelationId) : ICommand;

public record CompleteSaga(Guid Id, Guid CorrelationId) : ICommand;

public class ActivateLicenseSaga : Saga<ActivateLicenseSagaState>,
    IStartedBy<StartActivateLicenseSaga>,
    IHandleMessage<ActivateLicense>,
    IHandleMessage<PublishMovie>,
    IHandleMessage<CompleteSaga>
{
    private readonly ILicenseService _licenseService;
    private readonly IMovieService _movieService;

    public ActivateLicenseSaga(ActivateLicenseSagaState state,
        ILicenseService licenseService,
        IMovieService movieService) : base(state)
    {
        _licenseService = licenseService;
        _movieService = movieService;
    }

    public Task HandleAsync(IMessageContext<StartActivateLicenseSaga> context, CancellationToken cancellationToken = new CancellationToken())
    {
        State.LicenseId = context.Message.LicenseId;
        State.MovieId = context.Message.MovieId;
        State.RegionalIdentifier = context.Message.RegId;
        
        Publish(new ActivateLicense(Guid.NewGuid(), context.Message.CorrelationId));
        
        return Task.CompletedTask;
    }

    public async Task HandleAsync(IMessageContext<ActivateLicense> context, CancellationToken cancellationToken = new CancellationToken())
    {
        await _licenseService.Active(State.LicenseId);
        
        Publish(new PublishMovie(Guid.NewGuid(), context.Message.CorrelationId));
    }

    public Task HandleAsync(IMessageContext<PublishMovie> context, CancellationToken cancellationToken = new CancellationToken())
    {
        throw new NotImplementedException();
    }

    public Task HandleAsync(IMessageContext<CompleteSaga> context, CancellationToken cancellationToken = new CancellationToken())
    {
        State.MarkAsCompleted();
        
        return Task.CompletedTask;
    }
}