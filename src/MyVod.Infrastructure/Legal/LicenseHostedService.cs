using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyVod.Domain.BusinessProcesses.ActivateLicense;
using MyVod.Domain.Legal.Application;
using OpenSleigh.Core.Messaging;

namespace MyVod.Infrastructure.Legal;

public class LicenseHostedService : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;

    public LicenseHostedService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(1000, stoppingToken);

            using var scope = _serviceProvider.CreateScope();

            // var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();
            var licenseService = scope.ServiceProvider.GetRequiredService<ILicenseService>();
            var messageBus = scope.ServiceProvider.GetRequiredService<IMessageBus>();

            var readForActivate = await licenseService.FetchReadyToActivate();

            foreach (var license in readForActivate)
            {
                var startSagaEvent = new StartActivateLicenseSaga(Guid.NewGuid(),
                    Guid.NewGuid(),
                    license!.Id,
                    license.MovieId,
                    license.RegionalIdentifier);

                await messageBus.PublishAsync(startSagaEvent, CancellationToken.None);
                // await licenseService.Active(license.Id);
            }
        }
    }
}