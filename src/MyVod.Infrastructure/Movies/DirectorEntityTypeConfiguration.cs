using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.MoviesCatalog.Domain;

namespace MyVod.Infrastructure.Movies;

public class DirectorEntityTypeConfiguration : IEntityTypeConfiguration<Director>
{
    public void Configure(EntityTypeBuilder<Director> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
            .HasConversion(x => x.Value, s => new DirectorId(s));

        builder.Property(x => x.FirstName);
        builder.Property(x => x.LastName);
        
        builder.ToTable("Persons");
    }
}