using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.MoviesCatalog.Domain;
using MyVod.Domain.MoviesCatalog.Infrastructure;

namespace MyVod.Infrastructure.Movies;

public class MovieRepository : IMoviesRepository
{
    private readonly MoviesDbContextV2 _context;

    public MovieRepository(MoviesDbContextV2 context)
    {
        _context = context;
    }

    public Task<Movie?> Get(MovieId id)
    {
        return _context.Movies.Include(x => x.Director)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public void Update(Movie movie)
    {
        _context.Movies.Update(movie);
    }

    // public async Task Save(CancellationToken ct)
    // {
    //     await _context.SaveEntitiesAsync(ct);
    // }

    public IUnitOfWork UnitOfWork => _context;
}