using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.EFCore;
using MyVod.Domain.MoviesCatalog.Domain;

namespace MyVod.Infrastructure.Movies;

public class MoviesDbContextV2 : DbContext, IUnitOfWork
{
    public DbSet<Movie> Movies { get; private set; }
    
    public MoviesDbContextV2(DbContextOptions<MoviesDbContextV2> options) : base(options)
    {
        
    }

    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveChangesAsync(cancellationToken);

        return true;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new MovieEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new DirectorEntityTypeConfiguration());

        base.OnModelCreating(modelBuilder);
    }
}