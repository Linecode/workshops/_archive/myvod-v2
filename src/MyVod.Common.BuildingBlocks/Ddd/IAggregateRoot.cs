namespace MyVod.Common.BuildingBlocks.Ddd;

// ReSharper disable once InconsistentNaming
public interface IAggregateRoot<out TIdentifier>
{ 
    TIdentifier Id { get; }
}