using MyVod.Common.BuildingBlocks.EFCore;

namespace MyVod.Common.BuildingBlocks.Ddd;

public interface IRepository
{
    public IUnitOfWork UnitOfWork { get; }
}
